(ns lab3.core-test
  (:require
    [clojure.test :refer :all]
    [clojure.test.check :as tc]
    [clojure.test.check.generators :as gen]
    [clojure.test.check.properties :as prop]
    [lab3.core :refer :all]
    [lab3.cubic-spline :refer :all]
    [lab3.linear :refer :all]))


(def input-1 [[-5 10] [-3 6] [3 6] [5 10]])
(def input-2 [[0 1] [1 2] [2 3] [3 4]])
(def input-3 [[20 19] [32 35] [43 19] [59 4]])


(deftest data-generate-test
  (testing
    (reset! step 0.5)
    (is (= (data-generate -5 5) [-5 -4.5 -4.0 -3.5 -3.0 -2.5 -2.0 -1.5 -1.0 -0.5 0.0 0.5 1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5])))
  (reset! step 1)
  (is (= (data-generate -10 10) [-10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9]))
  (reset! step 5)
  (is (= (data-generate 11 11) [])))


(deftest linear-predict-test
  (testing
    (do
      (reset! step 0.5)
      (reset! train-points input-1)
      (let [sorted-train (vec (sort-by first @train-points))]
        (data-generate (first (get sorted-train 0)) (first (get sorted-train 1)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [10 9.0 8.0 7.0]))
        (data-generate (first (get sorted-train 1)) (first (get sorted-train 2)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [6 6.0 6.0 6.0 6.0 6.0 6.0 6.0 6.0 6.0 6.0 6.0]))
        (data-generate (first (get sorted-train 2)) (first (get sorted-train 3)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [6 7.0 8.0 9.0]))))
    (do
      (reset! step 0.2)
      (reset! train-points input-2)
      (let [sorted-train (vec (sort-by first @train-points))]
        (data-generate (first (get sorted-train 0)) (first (get sorted-train 1)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [1 1.2 1.4 1.6 1.8]))
        (data-generate (first (get sorted-train 1)) (first (get sorted-train 2)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [2 2.2 2.4 2.5999999999999996 2.8 3.0]))
        (data-generate (first (get sorted-train 2)) (first (get sorted-train 3)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [3 3.2 3.4000000000000004 3.6000000000000005 3.8000000000000007]))))
    (do
      (reset! step 4)
      (reset! train-points input-3)
      (let [sorted-train (vec (sort-by first @train-points))]
        (data-generate (first (get sorted-train 0)) (first (get sorted-train 1)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [19N 73/3 89/3]))
        (data-generate (first (get sorted-train 1)) (first (get sorted-train 2)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [35N 321/11 257/11]))
        (data-generate (first (get sorted-train 2)) (first (get sorted-train 3)))
        (is (= (mapv (linear-interpolator sorted-train) @data-points) [19N 61/4 23/2 31/4]))))))


(deftest spline-predict-test
  (testing
    (do
      (reset! step 1)
      (reset! train-points input-1)
      (let [sorted-train (vec (sort-by first @train-points))]
        (data-generate (first (get sorted-train 0)) (first (get sorted-train 1)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [10.0 7.863636363636364]))
        (data-generate (first (get sorted-train 1)) (first (get sorted-train 2)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [6.0 4.636363636363637 3.818181818181819 3.545454545454546 3.8181818181818192 4.636363636363638]))
        (data-generate (first (get sorted-train 2)) (first (get sorted-train 3)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [6.0 7.863636363636363]))))
    (do
      (reset! step 0.1)
      (reset! train-points input-2)
      (let [sorted-train (vec (sort-by first @train-points))]
        (data-generate (first (get sorted-train 0)) (first (get sorted-train 1)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.7999999999999998 1.9 2.0]))
        (data-generate (first (get sorted-train 1)) (first (get sorted-train 2)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [2.0 2.1 2.2 2.3000000000000003 2.4000000000000004 2.5000000000000004 2.6000000000000005 2.7000000000000006 2.8000000000000007 2.900000000000001]))
        (data-generate (first (get sorted-train 2)) (first (get sorted-train 3)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [3.0 3.1 3.2 3.3000000000000003 3.4000000000000004 3.5000000000000004 3.6000000000000005 3.7000000000000006 3.8000000000000007 3.900000000000001]))))
    (do
      (reset! step 3)
      (reset! train-points input-3)
      (let [sorted-train (vec (sort-by first @train-points))]
        (data-generate (first (get sorted-train 0)) (first (get sorted-train 1)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [19.0 25.231427090947562 30.5702833455161 34.123997927326585]))
        (data-generate (first (get sorted-train 1)) (first (get sorted-train 2)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [35.0 32.673326385075704 28.089444535766617 22.56142956320408]))
        (data-generate (first (get sorted-train 2)) (first (get sorted-train 3)))
        (is (= (mapv (spline-interpolator sorted-train) @data-points) [18.999999999999993 14.558643492959643 11.128301369599509 8.475662618301083 6.367416227445851 4.570251185415308]))))))
