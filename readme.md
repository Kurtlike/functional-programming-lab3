
## Функциональное програмирование
#### Лабараторная работа №3
#### Рождественский Н.С., P34102
**Дисциплина:** "Функциональное программирование"

**Выполнил:** Рождественский Никита, P34102

**Вариант:**
Spline-interpolation

**Требования:**
1. должна быть реализована «аппроксимация» отрезками
2. настройки алгоритма аппроксимирования и выводимых данных должны задаваться через аргументы командной строки:
  * какой или какие алгоритмы использовать
  * частота дискретизации
  * и т.п.
3. входные данные должны задаваться в текстовом формате на подобии “.csv” (к примеру `x;y\n` или `x\ty\n` и т.п.) и поддаваться на стандартный ввод,
входные данные должны быть отсортированы по возрастанию x
4. выходные данные должны подаваться на стандартный вывод
5. программа должна работать в потоковом режиме (пример - cat | grep 11)

### Логика работы  

Программа постоянно считает поток входящих строчек с stdin, и сразу же отвечает на них в stdout.
Внутри работает все на async/chan.

1) В аргументы программы передается шаг генератора.
2) На стандартный вход подаются точки для последующей аппроксимации.
3) Как только набирается минимальное окно (3 точки), программа рассчитывает аппроксимирующую функцию на данном отрезке и генерирует вывод, начиная от первой точки с шагом, заданным через аргументы.
4) При последующих добавлениях точек окно смещается и старые точки удаляются.
5) Генератор работает на оптимальном для функции отрезке.
6) При завершении программы вычисляются точки до последней введенной.

#### Настройка Clojure-окружения
```
{:paths ["src"]
    :deps
        {org.clojure/clojure {:mvn/version "1.11.1"}
        org.clojure/tools.cli {:mvn/version "1.0.214"}
        org.clojure/test.check {:mvn/version "1.1.1"}
        org.clojure/core.async {:mvn/version "1.4.627"}}
:aliases{
	:test
 {:extra-paths ["test"]
  :extra-deps {com.cognitect/test-runner
               {:git/url "https://github.com/cognitect-labs/test-runner.git"
                :sha "209b64504cb3bd3b99ecfec7937b358a879f55c1"}
               org.clojure/test.check {:mvn/version "1.1.1"}}
  :main-opts ["-m" "cognitect.test-runner"]}
:run
  {:extra-paths ["src"]
  :main-opts ["-m" "lab3.core"]}
:lint
  {:extra-paths ["src" "test"]
  :extra-deps {cljfmt/cljfmt {:mvn/version "0.9.0"}}
  :main-opts ["-m" "cljfmt.main check"]}
:lint_fix
  {:extra-paths ["src" "test"]
  :extra-deps {cljfmt/cljfmt {:mvn/version "0.9.0"}}
  :main-opts ["-m" "cljfmt.main fix"]}
  }}
```

#### Ввод/вывод
```
(ns lab3.io
  (:require [clojure.core.async
             :as a
             :refer [>! <! go-loop chan close!]]))

(defn input-reader [inp-chan]
  (go-loop [counter 0]
    (if-let [next-line (read-line)]
      (if-let [_ (>! inp-chan next-line)]
        (recur (inc counter))
        (println "channel closed")) ()))
  inp-chan)

(defn final-handler [inp-cs]
  (go-loop []
    (if-let [_ (<! inp-cs)]
      (recur)
      (println "out channel closed"))))

(defn updater [inp-chan func]
  (let [opt-chan (chan)]
    (go-loop []
      (if-let [next-val (<! inp-chan)]
        (do
          (func next-val)
          (>! opt-chan next-val)
          (recur))
        (close! opt-chan))) opt-chan))
```
#### Пример работы
```
kurtlike@debian:~/func/lab3$ cat input | clj -Mrun 0.5
Step: 0.5
need more dots
need more dots
linear approximated: -6.00 , 10.00 |-5.50 , 8.00 |
spline approximated: -6.00 , 10.00 |-5.50 , 7.88 |
linear approximated: -5.00 , 6.00 |-4.50 , 5.33 |-4.00 , 4.67 |-3.50 , 4.00 |-3.00 , 3.33 |-2.50 , 2.67 |
spline approximated: -5.00 , 6.00 |-4.50 , 5.19 |-4.00 , 4.41 |-3.50 , 3.68 |-3.00 , 3.02 |-2.50 , 2.45 |
linear approximated: -2.00 , 2.00 |-1.50 , 2.00 |-1.00 , 2.00 |-0.50 , 2.00 |0.00 , 2.00 |0.50 , 2.00 |1.00 , 2.00 |1.50 , 2.00 |
spline approximated: -2.00 , 2.00 |-1.50 , 1.81 |-1.00 , 1.64 |-0.50 , 1.51 |0.00 , 1.43 |0.50 , 1.42 |1.00 , 1.50 |1.50 , 1.69 |
linear approximated: 2.00 , 2.00 |2.50 , 2.67 |3.00 , 3.33 |3.50 , 4.00 |4.00 , 4.67 |4.50 , 5.33 |
spline approximated: 2.00 , 2.00 |2.50 , 2.18 |3.00 , 2.44 |3.50 , 2.88 |4.00 , 3.56 |4.50 , 4.57 |
^Clinear approximated: 5.00 , 6.00 |5.50 , 8.00 |
spline approximated: 5.00 , 6.00 |5.50 , 7.88 |
Shutting down!!!
```
#### Пример работы в ручном режиме
```
kurtlike@debian:~/func/lab3$ clj -Mrun 0.5
Step: 0.5
0,1
need more dots
3,8
need more dots
5,3
linear approximated: 0.00 , 1.00 |0.50 , 2.17 |1.00 , 3.33 |1.50 , 4.50 |2.00 , 5.67 |2.50 , 6.83 |
spline approximated: 0.00 , 1.00 |0.50 , 2.87 |1.00 , 4.62 |1.50 , 6.13 |2.00 , 7.28 |2.50 , 7.94 |
6,6
linear approximated: 3.00 , 8.00 |3.50 , 6.75 |4.00 , 5.50 |4.50 , 4.25 |
spline approximated: 3.00 , 8.00 |3.50 , 5.89 |4.00 , 4.13 |4.50 , 3.05 |
10,6
linear approximated: 5.00 , 3.00 |5.50 , 4.50 |
spline approximated: 5.00 , 3.00 |5.50 , 4.61 |
11,4
linear approximated: 6.00 , 6.00 |6.50 , 6.00 |7.00 , 6.00 |7.50 , 6.00 |8.00 , 6.00 |8.50 , 6.00 |9.00 , 6.00 |9.50 , 6.00 |
spline approximated: 6.00 , 6.00 |6.50 , 6.39 |7.00 , 6.75 |7.50 , 7.03 |8.00 , 7.20 |8.50 , 7.22 |9.00 , 7.05 |9.50 , 6.66 |
linear approximated: 10.00 , 6.00 |10.50 , 5.00 |
spline approximated: 10.00 , 6.00 |10.50 , 5.08 |
Shutting down!!!
```
Видно, что апроксимация происходит в оконе и программа работает в потоковом режиме.
