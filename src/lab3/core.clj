(ns lab3.core
  (:require
    [clojure.core.async
     :refer [chan close!]]
    [lab3.cubic-spline :refer [spline-interpolator]]
    [lab3.io :refer [input-reader final-handler updater]]
    [lab3.linear :refer [linear-interpolator]]))


(def train-points (atom []))
(def data-points (atom []))
(def step (atom 1))
(def state (atom 1))
(def window 3)


(defn data-update
  [vec new-dot]
  (if (>= (count vec) window)
    (let [sorted-vec (sort-by first vec)
          [_ & new-vec] sorted-vec]
      (conj new-vec new-dot))
    (conj vec new-dot)))


(defn add-train-point
  [line]
  (let [split (vec (.split line ","))
        [x_s y_s] split
        x (Double/parseDouble x_s)
        y (Double/parseDouble y_s)]
    (-> train-points
        (swap! data-update [x y]))))


(defn data-generate
  [start end]
  (let [generated (reduce #(conj %1 %2) [] (range start end @step))
        alt-fn (fn [_ new-vec] new-vec)]
    (-> data-points
        (swap! alt-fn generated))))


(defn print-answer
  [answers]
  (doseq [i (range (count @data-points))]
    (print  (format "%.2f" (get @data-points i)) "," (format "%.2f" (get answers i)) "|"))
  (println))


(defn predict
  [_]
  (let [sorted-train (vec (sort-by first @train-points))]
    (if (>= (count @train-points) window)
      (do
        (case @state
          1  (data-generate (first (get sorted-train 0)) (first (get sorted-train 1)))
          2  (data-generate (first (get sorted-train 1)) (first (get sorted-train 2))))
        (let [data @data-points
              linear-resolved (future (mapv (linear-interpolator sorted-train) data))
              cubic-spline-resolved (future (mapv (spline-interpolator sorted-train) data))]
          (print "linear approximated: ")
          (print-answer @linear-resolved)
          (print "spline approximated: ")
          (print-answer @cubic-spline-resolved)))
      (println "need more dots"))))


(def main_chan (chan))
(def out_chan (chan))


(defn shutdown-guard
  [effect]
  (let [shutdown-p (promise)
        hook-f (fn []
                 (effect)
                 (println "Shutting down!!!")
                 (deliver shutdown-p 0))]
    (.addShutdownHook (Runtime/getRuntime)
                      (Thread. ^Runnable hook-f))
    (System/exit @shutdown-p)))


(defn println-builder
  [message]
  (fn [value] (println message value)))


(defn -main
  [& args]
  (let [init-step (read-string (first args))]
    (reset! step init-step)
    (println "Step:" @step)
    (-> main_chan
        (input-reader)
        (updater add-train-point)
        (updater predict)
        (final-handler)))
  (shutdown-guard #(do
                     (reset! state 2)
                     (predict 2)
                     (close! main_chan))))
