(ns lab3.io
  (:require
    [clojure.core.async
     :as a
     :refer [>! <! go-loop chan close!]]))


(defn input-reader
  [inp-chan]
  (go-loop [counter 0]
    (if-let [next-line (read-line)]
      (if-let [_ (>! inp-chan next-line)]
        (recur (inc counter))
        (println "channel closed")) ()))
  inp-chan)


(defn final-handler
  [inp-cs]
  (go-loop []
    (if-let [_ (<! inp-cs)]
      (recur)
      (println "out channel closed"))))


(defn updater
  [inp-chan func]
  (let [opt-chan (chan)]
    (go-loop []
      (if-let [next-val (<! inp-chan)]
        (do
          (func next-val)
          (>! opt-chan next-val)
          (recur))
        (close! opt-chan))) opt-chan))
